package sks.sergey.chat.server;

import sks.sergey.chat.network.TCPConnection;
import sks.sergey.chat.network.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

/**
 * Created by SKS on 06.09.2017.
 */
public class ChatServer implements TCPConnectionListener{

    public static void main(String[] args){
        new ChatServer();                                               // создаем и запускаем сервер
    }

    private final ArrayList<TCPConnection> connections = new ArrayList<>(); // список соединений

    private ChatServer(){
        System.out.println("Server runnig...");                         // в консоль пишем сервер запустился
        try(ServerSocket serverSocket = new ServerSocket(8888)){   // слушает порт и входящее соединение
            while (true){                                               // принимает все входящие соединения
                try{
                    new TCPConnection(this, serverSocket.accept()); // передали себя и объект socket
                } catch (IOException e){
                    System.out.println("TCPConnection: " + e);

                }
            }
        } catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    // synchronized что бы нельзя было попасть из разных методово
    public synchronized void onConnectionReady(TCPConnection tcpConnection) {
        connections.add(tcpConnection);                                           // добавляем в список соединений
        sendToAllConnections("Client connected: " + tcpConnection);         // всем сообщаем подключился новый
    }

    @Override
    public synchronized void onReceiveString(TCPConnection tcpConnection, String value) {
        sendToAllConnections(value);                                              // всем отправим сообщение
    }

    @Override
    public synchronized void onDisconnect(TCPConnection tcpConnection) {
        connections.remove(tcpConnection);                                           // удаляем из списка соединений
        sendToAllConnections("Client disconnected: " + tcpConnection);         // всем сообщаем кто отключился
    }

    @Override
    public synchronized void onException(TCPConnection tcpConnection, Exception e) {
        System.out.println("TCPConnection exception: " + e);                         // пишем в консоль ошибку
    }

    // метод отправить всем в чате сообщение
    private void sendToAllConnections(String value){
        System.out.println(value);                                          // вывод в консоль сообщения
        final int cnt = connections.size();                                 // кол-во подключившихся
        for (int i = 0; i < cnt;  i++) {                                    // проходим по списку и всем отправляем
           connections.get(i).sendString(value);
        }
    }
}
