package sks.sergey.chat.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 * Created by SKS on 07.09.2017.
 */
public class TCPConnection {

    private final Socket socket;                                // сокет связанный с соединением
    private final Thread rxThread;                              // поток слушающий входящие соединения
    private final BufferedReader in;                            // для потока ввода работающий со строками
    private final BufferedWriter out;                           // для потока вывода
    private final TCPConnectionListener eventListener;          // слушатель событий

    // конструктор 2, socet создается из нутри, спросить eventListener + ip адресс + порт
    public TCPConnection(TCPConnectionListener eventListener, String ipAddr, int port)throws IOException{
        this(eventListener, new Socket(ipAddr, port));          // вызываем конструктор 1, передаем туда
    }

    // конструктор 1, кто то с наружи создаст socet на вход слушатель + сокет, генер исключение
    public TCPConnection (TCPConnectionListener eventListener, Socket socket) throws IOException{
        this.eventListener = eventListener;                         // запоминаем слушателя
        this.socket = socket;                                       // запоминаем входящий сокет

        // поток ввода  и вывода, слушащий сокет, преоброзуем к строке, с жесткой кодировкой
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));

        rxThread = new Thread(new Runnable() {  // создаем новый поток, через анонимный класс, передаем интерф Runnable
            @Override                           // Override run() и создаем его экземпляр
            public void run() {                 // запускаем метод run(), слушаем входящее соединение
                try {
                    eventListener.onConnectionReady(TCPConnection.this);    // передали экз управл класса
                    while (!rxThread.isInterrupted()) {                                 // пока есть соединение
                        //String msg = in.readLine();                                   // слушаем входящее соединение
                        //eventListener.onReceiveString(TCPConnection.this, msg);       //  отдаем его
                        eventListener.onReceiveString(TCPConnection.this, in.readLine());   // сокращаем
                    }
                } catch (IOException e) {
                    eventListener.onException(TCPConnection.this, e);                   // если случилось говорим о нем
                } finally {
                    eventListener.onDisconnect(TCPConnection.this);
                }

            }
        });
        rxThread.start();                                                       // и запускаем его
    }

    public synchronized void sendString(String value){                           // метод отправки сообщения
        try {
            out.write(value + "\r\n");                                      // написали сообщение
            out.flush();                                                        // сбрасывает с буфера и отправляет
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);                   // если случилось говорим о нем
            disconnect();                                                       // разрываем соединение
        }
    }

    public synchronized void disconnect(){                                      // метод разрыва соединения
        rxThread.interrupt();                                                   // прервать соединение
        try {
            socket.close();                                                     // закрыть сокет
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
        }
    }

    @Override
    public String toString(){                                                   // сообщает кто подключился/отключился
        return "TCPConnection" + socket.getInetAddress() + ": " + socket.getPort();
    }
}
