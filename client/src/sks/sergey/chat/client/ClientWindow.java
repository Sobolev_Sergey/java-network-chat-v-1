package sks.sergey.chat.client;

import javafx.scene.layout.Border;
import sks.sergey.chat.network.TCPConnection;
import sks.sergey.chat.network.TCPConnectionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by SKS on 06.09.2017.
 */
public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener{

    private static String IP_ADDR = "127.0.0.1";                            // ip адрес сервера
    private static int PORT = 8888;                                         // порт сервера
    private static int WIDTH = 600;                                         // ширина окна
    private static int HEIGHT = 600;                                        // высота окна
    private final JTextArea log = new JTextArea();                          // окно с сообщениями
    private final JTextField fieldNickname = new JTextField("Sergey");  // окно для ввода имени
    private final JTextField fieldInput = new JTextField();                  // окно для ввода сообщений
    private TCPConnection connection;                                        // объявляем соединение

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {                     // заставляем swing запуст в другом потоке edt
            @Override
            public void run() {
                new ClientWindow();                                     // создаем окно
            }
        });
    }



    private ClientWindow(){
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // на крестик окно и программа закрылись
        setSize(WIDTH, HEIGHT);                                     // устанавливаем окно
        setLocationRelativeTo(null);                                // окно всегда посередине
        setAlwaysOnTop(true);                                       // окно всегда сверху

        log.setEditable(false);                                     // запрет редактирования в окне
        log.setLineWrap(true);                                      // автоматический перенос слов
        add(log, BorderLayout.CENTER);                              // добавляем окно в центр

        fieldInput.addActionListener(this);                      // перехватываем сообщение
        add(fieldInput, BorderLayout.SOUTH);                        // добавляем окно ввода на юг
        add(fieldNickname, BorderLayout.NORTH);                     // добавляем окно ввода имени на север

        setVisible(true);                                           // окно делаем видимым

        // создаем соединение для использования
        try {
            connection = new TCPConnection(this, IP_ADDR, PORT);
        } catch (IOException e) {
            printMsg("Connection exception: " + e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {    // реализуем интерфейс нажатия клавиши Enter для отправки сообщения
        String msg = fieldInput.getText();          // получаем текст сообщения
        if(msg.equals("")) return;                  // проверяем отправку пустого сообщения, если пусто не отправляем
        fieldInput.setText(null);                   // стираем
        connection.sendString(fieldNickname.getText() + ": " + msg);             // передаем наше сообщение
    }

    // эти методы не синхронизируем, т.к. они вызываются из одного потока, используем одно соединение
    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMsg("Connection ready...");
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMsg(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMsg("Connection close...");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        printMsg("Connection exception: " + e);
    }

    // метод для написания сообщения
    private synchronized void printMsg(String msg){
        SwingUtilities.invokeLater(new Runnable() {                     // заставляем swing запуститься в другом потоке
            @Override
            public void run() {
                log.append(msg + "\n");                                 // добавляем сообщение которое к нам пришло
                // способ 100% перемещения корретки, текста в окне
                log.setCaretPosition(log.getDocument().getLength());
            }
        });
    }
}
