package sks.sergey.chat.network;

/**
 * Created by SKS on 07.09.2017.
 */
public interface TCPConnectionListener {

    void onConnectionReady(TCPConnection tcpConnection);                    // событие соединение готово
    void onReceiveString(TCPConnection tcpConnection, String value);        // событие приняли строчку, узнать какую
    void onDisconnect(TCPConnection tcpConnection);                         // событие соединение разорвано
    void onException(TCPConnection tcpConnection, Exception e);             // событие исключение, узнать какое

}
